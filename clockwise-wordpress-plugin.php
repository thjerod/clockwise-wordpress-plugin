<?php
   /*
   Plugin Name: TeamHealth Clockwise
   Plugin URI: http://teamhealth.com
   description: Shortcode implemenation of Clockwise wait time and appointment scheduling
   Version: 1.0.2
   Author: Jerod Mills
   Author URI: http://teamhealth.com
   License: GPL2
   */
   
   
    //Add the shortcode clockwise_waittime to our functions
	add_shortcode( 'clockwise_waittime', 'teamhealth_clockwise_shortcode' );
	//Function to handle shortcode clockwise_waittime
	function teamhealth_clockwise_shortcode( $attributes ) {
		
		//Get all the shortcodes and make them lowercase in case somebody uses incorrect case in parameters
		$attributes = array_change_key_case((array)$attributes, CASE_LOWER);
		//Our default shortcode values. Will default to the value on the right if they are not set.
		$th_atts = shortcode_atts([
				 'id' => '',
                 'closedmessage' => 'The clinic is currently closed.',
                 'range' => 15,
                 'wrapper' => 'span',
                 'class' => '',
                 'style' => ''
             ], $attributes);
        //Add slashes to our paamters so when we write them back to the DOM you can use double or single quotes
        array_walk_recursive($th_atts, function(&$item, $key) {
			$item = addslashes($item);
		});
		//The HTML to return. You'll notice the parameeters from above in the HTML
		$o = <<<HTML
	<{$th_atts['wrapper']} class="{$th_atts['class']}" id="current-wait-{$th_atts['id']}" style="{$th_atts['style']}"></{$th_atts['wrapper']}>
	<script type='application/javascript'>
		  function waitTimeMessage_{$th_atts['id']}(rawWait){
	        if (rawWait === 'Closed' || rawWait === 'N/A') {
	          return '{$th_atts['closedmessage']}'
	        };
	        var numericWait = parseInt(rawWait);
	        var waitRangeEnd = numericWait + {$th_atts['range']};
	        return  numericWait + ' - ' + waitRangeEnd + ' Minutes';
		 };
	
	 
	  		document.addEventListener("DOMContentLoaded", function () { 
	      		jQuery(function($) {
		      	   var WAIT_FETCH_OBJECTS_{$th_atts['id']} = [
			          { hospitalId: {$th_atts['id']},
			            timeType:       'hospitalWait',
			            selector:       '#current-wait-{$th_atts['id']}',
			            formatFunction: waitTimeMessage_{$th_atts['id']} }
			      ];
			      beginWaitTimeQuerying(WAIT_FETCH_OBJECTS_{$th_atts['id']});
				 });
			});
		
	</script>  


HTML;
		//Only return stuff if there is an ID set because the javascript needs an ID to lookup a wait time 
		if($th_atts['id'] != ""){
			//Load the javascripts on the page
			wp_enqueue_script( 'jquery-js', 'https://code.jquery.com/jquery-3.4.1.min.js','','',true );
			wp_enqueue_script( 'clockwise-js', 'https://s3-us-west-1.amazonaws.com/clockwisepublic/clockwiseWaitTimes.min.js','','',true );
			//Return our output after clearning any p tags
			return tgm_io_shortcode_empty_paragraph_fix($o);
		}
	}
	
	
	//Add the shortcode for the clockwise_button
	add_shortcode( 'clockwise_button', 'teamhealth_clockwise__button_shortcode' );
	//Function to handle the shortcode clockwise_button
	function teamhealth_clockwise__button_shortcode( $attributes ) {
		//Get all the shortcodes and make them lowercase in case somebody uses incorrect case in parameters
		$attributes = array_change_key_case((array)$attributes, CASE_LOWER);
		//Our default shortcode values. Will default to the value on the right if they are not set.
		$th_atts = shortcode_atts([
				 'class' => 'button',
                 'text' => 'HOLD MY SPOT IN LINE',
                 'id' => '',
                 'style' => ''
             ], $attributes);
        //The HTML to return. You'll notice the parameeters from above in the HTML 
        $o = "<a class='{$th_atts['class']}' href='https://www.clockwisemd.com/hospitals/{$th_atts['id']}/appointments/new' target='_blank' rel='noopener' style='{$th_atts['style']}'>{$th_atts['text']}</a>";
        //Only return stuff if there is an ID set. 
        if($th_atts['id'] != "")
       		return $o;
    }
	
	function tgm_io_shortcode_empty_paragraph_fix( $content ) {
 
	    $array = array(
	        '<p>['    => '[',
	        ']</p>'   => ']',
	        ']<br />' => ']'
	    );
	    return strtr( $content, $array );
	 
	}
	
?>